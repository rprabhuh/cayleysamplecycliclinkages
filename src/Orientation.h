#ifndef ORIENTATION_H_
#define ORIENTATION_H_

#include<vector>
#include<unordered_map>
/*!
 * \brief class for orientations or realizations of Cayley points
 *
 * Each Cayley point has several realizations or orientations which 
 * are stored as a vector in the CayleyPoint class.
 *
 * \sa CayleyPoint.h
 */
class Orientation {
public:
	/*!
	 *\brief Orientation default Constructor
 	 */
	Orientation();

	/*!
	 *\brief Orientation destructor
	 * \note Clears all the vectors inside
 	 */
	~Orientation();

	/*!
	 *\brief return the flip number computed only using the tetrahedra
	 *
	 * \return tetraFlipNum
 	 */
	int getTetraFlipNum();

	/*!
	 *\brief return the global flip number 
	 *
	 * \return subFlipNum
 	 */
	int getSubFlipNum();
	
	/*!
	 *\brief Returns the Cayley parameter values of the free Cayley parameters
	 *
	 * \return A vector of Cayley parameter values of the free Cayley parameters
 	 */
	std::vector<double> getFreeCayeleyParameters();
	
	/*!
	 *\brief Returns the Cayley parameter values of the global Cayley parameters
	 *
	 * \return A vector of Cayley parameter values of the global Cayley parameters
 	 */
	std::vector<double> getGlobalRigidityParameters();
	
	/*!
	 *\brief Set the tetrahedral flip number
	 *
	 * \param[in] [tetraFlipNum] [Tetrahedral flip number of the orientation]
 	 */
	void setTetraFlipNum(int tetraFlipNum);

	/*!
	 *\brief Set the sub flip number
	 *
	 * \param[in] [subFlipNum] [Sub flip number of the orientation]
 	 */
	void setSubFlipNum(int subFlipNum);

	/*!
	 *\brief Sets the Cayley parameter values of the free Cayley parameters
	 *
	 * \param[in] [freeCayleyParam] [Vector of Cayley parameter values of free Cayley parameters]
 	 */
	void setFreeCayeleyParameters(std::vector<double> freeCayleyParam);

	/*!
	 *\brief Sets the Cayley parameter values of the global Cayley parameters
	 *
	 * \param[in] [globalRigidityParameters] [Vector of Cayley parameter values of global Cayley parameters]
 	 */
	void setGlobalRigidityParameters(std::vector<double> globalRigidityParameters);

private:
	int tetraFlipNum;///<Tetrahdral flip number
	int subFlipNum;///<sub flip number
	std::vector<double> freeCayeleyParameters;///<Free Cayley parameters
	std::vector<double> globalRigidityParameters;///<Global Cayley parameters
	std::unordered_map<int, std::vector<double> > atomPositions;///The position of atoms in this orientation
};
#endif
