#ifndef CONSTRAINTGRAPH_H
#define CONSTRAINTGRAPH_H

#include <unordered_set>
#include <vector>
#include <unordered_map>
#include <boost/functional/hash.hpp>

/*!
 * \class ConstraintGraph
 *
 * \brief class for the constraint graph being sampled
 *
 * This class holds the costraint linkage being sampled.
 *
 * \author Rahul Prabhu
 * \date September 2019
 */
class ConstraintGraph {
public:
	/*!
	 * \brief Creates the constraint graph of the input cycloalkane
	 *
	 * \param[in] [numAtoms] [Number of atoms in the input cycloalkane]
	 * \return [Constraint graph of the input cycloalkane]
	 * \note Internally calls the private ConstraintGraph constructor to create the object
	 * \warning Currently only supports cycloalkanes with 7, 8, and 9 carbon atoms
	 */
	static ConstraintGraph* createCycloAlkane(int numAtoms);
	~ConstraintGraph();
	/*!
	 *\brief Drop edges to make the graph a partial 3-tree
	 *
	 * We drop edges to make the graph a partial 3-tree. Currently,
	 * the edges to be dropped are hard-coded for each of the
	 * supported graphs. Going forward, this will be replaced by a
	 * an algorithm that works for general cyclic graphs.
	 * 
	 * \note Currently only supports graphs with 7 or 8 vertices.
 	 */
	void drop_edges();

	/*!
	 *\brief Add Cayley parameters to make the graph a complete 3-tree.
	 *
	 * Add Cayley parameters to make the graph a complete 3-tree. 
	 * During the process, create a list of tetrahedra that are formed
	 * due to the addition of the Cayley parameters.
	 * 
	 * \note Currently only supports graphs with 7 vertices.
 	 */
	void add_cayleyParameters();


	//Getters and Setters
	/*!
	 *\brief return the number of vertices
	 *
	 * \return int
 	 */
	int getNumVertices(); 
	/*!
	 *\brief return the number of edges
	 *
	 * \return int
 	 */
	int getNumEdges();

	/*!
	 *\brief return the number Cayley parameters
	 *
	 * \return int
 	 */
	int getNumCayleyParameters(); 

	/*!
	 *\brief return the number dropped edges 
	 *
	 * \return int
 	 */
	int getNumDroppedEdges();

	/*!
	 *\brief return the list of tetrahedra 
	 *
	 * \return int
 	 */
	std::vector<std::vector<int> > getTetrahedra();

	/*!
	 *\brief return the edges of the current graph.
	 * 
	 * \return std::unordered_map<std::pair<int, int>, double, boost::hash<std::pair<int, int>> >
 	 */
	std::unordered_map<std::pair<int, int>, double, boost::hash<std::pair<int, int>> > getEdges(); 

	/*!
	 * \brief return the edge length of a given edge.
	 *
	 * \return double 
	 *
	 * \note We store edge lengths as an ordered pair (a,b). However, the
	 * edges in a constraint graph are not directed. So, we need to test 
	 * both (a, b) and (b, a) and return whichever is found.
	 */
	bool getEdgeLength(int, int, double*);
	
	/*!
	 *\brief return the list of dropped edges.
	 *
	 * \return std::vector<std::pair<int, int>> 
 	 */
	std::vector<std::pair<int, int>> getDroppedEdges();

	/*!
	 *\brief return the list of Cayley parameters
	 *
	 * \return std::vector<std::pair<int, int>> 
 	 */
	std::vector<std::pair<int, int>> getCayleyParameters();

	/*!
	 *\brief return the range of Cayley parameters
	 *
	 * \return std::vector<std::pair<int, int>> 
 	 */
	std::vector<std::pair<double, double>> getCayleyParameterRanges();

	/*!
	 * \brief Generate the Cayley parameter ranges for all Cayley parameters
	 *
	 * Using the theory outlined in the Master's thesis by Ugandhar Chittamuru, 
	 * we determine the parameter ranges of all the Cayley parameters.
	 *
	 * \return void
	 *
	 * \sa  Ugandhar's thesis \@ http://etd.fcla.edu/UF/UFE0042297/chittamuru_u.pdf
	 * \note In the case of cycloalkanes, all the Cayley parameters are parts
	 * of different tetrahedra that are symmetric. Thus, it is sufficient to compute
	 * the range of one Cayley parameter and reuse that for all other Cayley parametes.
	 * 
	 * In addition, all the Cayley parameters are non-sharing non-edges, thus
	 * their range can be computed by simply setting the volume of the tetrahedron to 0.
	 * Using the Cayley-Menger determinant, the volume of a tetrahedron is given
	 * by the following.
	 *
	 *  288V^2=|0     1 	   		1		   1 			1   | 
	 * 		   |1	  0			d_(10)^2	d_(20)^2	d_(30)^2| 
	 * 		   |1	d_(10)^2 		0 		d_(21)^2	d_(31)^2| 
	 * 		   |1	d_(20)^2 	d_(21)^2 		0		d_(32)^2| 
	 * 		   |1	d_(30)^2 	d_(31)^2 	d_(32)^2 	    0	| 
	 *
	 * Where, V is the volume of the tetrahedron. d_(ij) is the 
	 * distance between vertices i and j. We want to solve for one of the distances,
	 * say d_(34) by setting the volume V to 0. 
	 *  
	 * \warning We are assuming the input graph is a cyclic G^2 graph. If this changes,
	 * we'll need to rewrite this method to be more general.
 	 */
	void generateCayleyParamRanges();

	/*!
	 * \brief Gets the index of the Cayley parameter in the Constraint graph data structure
	 *
	 * \param[in] [v1] [First vertex of the edge]
	 * \param[in] [v2] [Second vertex of the edge]
	 *
	 * \return Index of the edge (v1, v2) in the cayley_parameters data structure
	 */
	int getCayleyParameterIndex(int v1, int v2);

private:

	int numVertices; ///< The number of Vertices

	std::unordered_set<int> vertices; ///< The set of vertices

	std::unordered_map<std::pair<int, int>, double, boost::hash<std::pair<int, int>> > edges; ///< Map of edges and their lengths

	std::vector<std::pair<int, int>> dropped_edges; ///< Vector of dropped edges

	std::vector<std::pair<int, int>> cayley_parameters; ///< Vector of Cayley parameters

	std::vector<std::pair<double, double>> cayleyParametersRanges; ///< Vector of Cayley parameter ranges

	std::vector<std::vector<int> > tetrahedra; ///< List of tetrahedra formed after the addition of Cayley parameters

	/*!
 	 * \brief ConstraintGraph private constructor
 	 *
	 * \param[in] [numVertices] [The number of vertiecs in the graph.]
	 * \param[in] [edges] [The list of edges in the graph.]
	 * \warning Do not make this public, please go through the static createCycloAlkane function to create an object
	 * \sa createCycloAlkane
	 */
	ConstraintGraph(int numVertices, std::vector<std::pair<int,int> > edges);

	/*!
 	 * \brief Initialze the edge lengths 
 	 *
 	 * The lengths of edges in the cycle are all unit distance
	 * but, the diagonal edges (derieved from the angle constraints)
	 * depend on the number of vertices. 
	 * We initialize the lengths using macros defined in the Utils class.
 	 *
	 * \param[in] edges The list of edges in the graph.
	 * \sa Utils.h
	 */
	void init_edge_lengths(std::vector<std::pair<int,int> > edges);
};
#endif
