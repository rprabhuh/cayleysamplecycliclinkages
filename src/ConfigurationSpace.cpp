#include <iostream>
#include <fstream>
#include <glog/logging.h>
#include "ConfigurationSpace.h"

ConfigurationSpace::ConfigurationSpace(ConstraintGraph *cg, double stepSize) {
	this->cg = cg;
	this->stepSize = stepSize;
	
	dimension = cg->getNumCayleyParameters();
}

ConfigurationSpace::~ConfigurationSpace() {
	for(CayleyPoint* point : space) {
		delete point;
	}
	space.clear();
}

std::vector<CayleyPoint*> ConfigurationSpace::getSpace() {
	return space;
}

int ConfigurationSpace::getDimension() {
	return dimension;
}


std::vector<std::vector<double>> ConfigurationSpace::convertToCartesianSpace() {
		std::vector<std::vector<double>> cartesianSpace;

		return cartesianSpace;
}

bool ConfigurationSpace::stepInCayleyGrid() {
	return false;
}

void ConfigurationSpace::gridSampleConfigurationSpace() {
	//While sampling make a note of points that match the lengths of the dropped edges
	std::vector<std::pair<int, int> > cayleyParameters = cg->getCayleyParameters();
	std::vector<std::pair<double, double>> cayleyParametersRanges = cg->getCayleyParameterRanges();
	std::vector<double> currentParameterValues;
	for(int i=0; i<dimension; i++) {
		currentParameterValues.push_back(cayleyParametersRanges[i].first);
	}

	std::fstream fout;
	fout.open("SamplePoints.txt", std::ios_base::app | std::ios_base::out);

	if(dimension == 4) {
		int ID = 0;
		while(currentParameterValues[0]< cayleyParametersRanges[0].second) {
			currentParameterValues[1] = cayleyParametersRanges[1].first;
			while(currentParameterValues[1]< cayleyParametersRanges[1].second) {
				currentParameterValues[2] = cayleyParametersRanges[2].first;
				while(currentParameterValues[2]< cayleyParametersRanges[2].second) {
					currentParameterValues[3] = cayleyParametersRanges[3].first;
					while(currentParameterValues[3] < cayleyParametersRanges[3].second) {
						CayleyPoint* cp = new CayleyPoint(ID++, currentParameterValues, cg);
						cp->computeRealizations(cg->getTetrahedra());
						space.push_back(cp);
						currentParameterValues[3]+=stepSize;
						fout<<*cp;
					}
					currentParameterValues[2]+=stepSize;
				}
				currentParameterValues[1]+=stepSize;
			} 
			currentParameterValues[0]+=stepSize;
		}
	} else if(dimension == 5) {
		int ID = 0;
		while(currentParameterValues[0]< cayleyParametersRanges[0].second) {
			currentParameterValues[1] = cayleyParametersRanges[1].first;
			while(currentParameterValues[1]< cayleyParametersRanges[1].second) {
				currentParameterValues[2] = cayleyParametersRanges[2].first;
				while(currentParameterValues[2]< cayleyParametersRanges[2].second) {
					currentParameterValues[3] = cayleyParametersRanges[3].first;
					while(currentParameterValues[3] < cayleyParametersRanges[3].second) {
						currentParameterValues[4] = cayleyParametersRanges[4].first;
						while(currentParameterValues[4] < cayleyParametersRanges[4].second) {
							CayleyPoint* cp = new CayleyPoint(ID++, currentParameterValues, cg);
							cp->computeRealizations(cg->getTetrahedra());
							space.push_back(cp);
							currentParameterValues[4]+=stepSize;
							fout<<*cp;
						}
						currentParameterValues[3]+=stepSize;
					}
					currentParameterValues[2]+=stepSize;
				}
				currentParameterValues[1]+=stepSize;
			} 
			currentParameterValues[0]+=stepSize;
		}
	} else  {
		std::cout<<"Operation Unsupported at this time."<<std::endl;
		VLOG(google::INFO)<<"Operation Unsupported at this time.";
	}
	fout.close();
}
