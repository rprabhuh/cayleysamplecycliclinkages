#ifndef CONFIGURATIONSPACE_H_
#define CONFIGURATIONSPACE_H_

#include<vector>
#include "CayleyPoint.h"
#include "ConstraintGraph.h"

/*!
 * \class ConfigurationSpace
 *
 * \brief class for the configuration space of the linkage being sampled
 *
 * This class holds the configuration space aka the set of Cayley sample points
 * for the current linkage being sampled. 
 *
 * \author Rahul Prabhu
 * \date September 2019
 */
class ConfigurationSpace {
public:
	/*!
	 *\brief Constructor for ConfigurationSpace class
	 *
	 * \param[in] cg The constraint graph to be sampled.
	 * \param[in] stepSize Sampling step size.
	 * 
	 * \note The dimension of the configuration space is computed simply
	 * as the number of Cayley parameters.
	 * 
	 * \warning Pass in an already constructed ConstraintGraph object.
 	 */
	ConfigurationSpace(ConstraintGraph *cg, double stepSize);

	/*!
	 *\brief Destructor for the Configuration space class
	 *
	 * The destructor deletes all the individual Cayley points and 
	 * clears the vector holding them.
	 *
	 * \warning Do not delete the ConstraintGraph object.
 	 */
	~ConfigurationSpace(); 

	/*!
	 *\brief Returns the vector of CayleyPoint objects
	 *
	 * \return vector<CayleyPoint*>
	 * \note [any note about the function you might have]
	 * \warning Will return an empty vector if the space hasn't been sampled.
 	 */
	std::vector<CayleyPoint*> getSpace();

	/*!
	 *\brief Returns the dimension of the configuration space.
	 *
	 * \return int
 	 */
	int getDimension();
	
	/*!
	 *\brief Project the Cayley points onto the Cartesian space.
	 *
	 * For each individual Cayley configuration, find the corresponding
	 * Cartesian coordinates.
	 *
	 * \return std::vector<std::vector<double>>
	 * \note Right now, I plan to return the position of the points in x, y, z and Cardan angles &theta, &phi, and &psi.
 	 */
	std::vector<std::vector<double>> convertToCartesianSpace();

	/*!
	 *\brief Grid sample the configuration space.
	 *
	 * Determine the order of Cayley parameters to sample. Determine
	 * the range of each Cayley parameter, grid sample the parameters
	 * using a user defined step size.
	 *
	 * \note This way of sampling may need to change. We will need to
	 * sample densely closer to solutions. 
 	 */
	void gridSampleConfigurationSpace();

	
private:
	std::vector<CayleyPoint*> space; ///<Vector of cayley points in the sapce
	int dimension; ///<Dimension of the Configuration space
	ConstraintGraph *cg;///<ConstraintGraph being sampled
	double stepSize;///<Sampling step size

	/*!
	 *\brief Takes one step in the Cayley grid.
	 *
	 * Helper function to gridSampleCayleyConfigurationSpace.
	 * Keeps track of the parameter being advanced and takes one
	 * step in that direction.
	 *
	 * \note This way of sampling may need to change, for dynamic step sizes.
	 * In cases such as sampling densely closer to solutions. 
 	 */
	  bool stepInCayleyGrid();

};

#endif
