#include<iostream>
#include<vector>

#include <boost/program_options.hpp>
#include <glog/logging.h>

#include "ConstraintGraph.h"
#include "ConfigurationSpace.h"
#include "Utils.h"

using namespace std;
namespace po = boost::program_options;

/*! \mainpage Cayley Sample Cyclic Linkages
 *
 * \section intro_sec Introduction
 *
 * This is a mathematical software that formally model the energy landscapes of cycloalkanes (monocyclic saturated hydorcarbons). 
 * Our novel geometric methodology is based on the theory of convex Cayley (distance-based) parametrization. 
 * Cycloalkane molecules are represented as bar-and-joint geometric constraint systems with the atoms being the joints, 
 * and the bondlengths and bond angles represent constraints between the atoms. 
 * Using this setup, the energy landscape of the cyclo alkane can be thought of as the
 * solution space or configuration space to this geometric constraint system.
 *
 * \section requirements_sec Requirements
 * These are configurations with which the software has been tested. It may work on other equivalent platforms/libraries, but hasn't been tested.
 * 		- Ubuntu 18.04 or higher
 * 		- CMake version 3.10.2 or higher
 * 			- sudo apt install cmake
 * 		- g++ version 9.2.1 or higher
 * 			- sudo apt install g++
 * 		- Boost c++ libraries version 1.4 or higher
 * 			- sudo apt install libboost-all-dev
 *
 * 	Optionally
 * 		- Google logging module glog version 0.4 or higher for logging
 * 			- https://codeyarns.com/2017/10/26/how-to-install-and-use-glog/
 * 		- Google Test framework for running test cases
 * 			- https://gist.github.com/Cartexius/4c437c084d6e388288201aadf9c8cdd5
 *
 * \section install_sec Installation
 * 		- $ mkdir build
 * 		- $ cd build
 * 		- $ cmake ..
 * 		- $ ./cycloalkane
 */

int main(int argc, char **argv) {

	//Handle Commandline arguments
	po::variables_map Vmap;
	int claStatus = handCommandLineArguments(argc, argv, Vmap);
	if(claStatus == 1) {
		return 1;
	}

	// Initialize Google's logging library.
    google::InitGoogleLogging(argv[0]);
    FLAGS_log_dir = ".";
    FLAGS_v = Vmap["log-level"].as<int>();

	//Create a graph
	ConstraintGraph *cg = ConstraintGraph::createCycloAlkane(Vmap["atoms"].as<int>());
	if(cg == nullptr) {
		LOG(INFO) << "Input cycloalkane currently  unsupported."<<
				" Try a different input molecule.";
		cout<<"Input cycloalkane currently  unsupported."<<
				" Try a different input molecule."<<endl;
		exit(1);
	}

	//Drop edges in the graph to make it a partial 3-tree
	cg->drop_edges();

	//Add non-edges to make it rigid and also a 3-tree
	cg->add_cayleyParameters();
	
	//Generate the range of Cayley parameters
	cg->generateCayleyParamRanges();

	//Get the step size from the commandline arguments
	double stepSize = Vmap["step-size"].as<double>();

	//Create ConfigurationSpace object
	ConfigurationSpace *confSpace = new ConfigurationSpace(cg, stepSize);

	//Grid sample the Cayley parameters
	confSpace->gridSampleConfigurationSpace();
	
	//Visualize the points

}
