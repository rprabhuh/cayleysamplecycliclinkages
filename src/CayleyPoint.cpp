#include <cmath>
#include <iostream>
#include "CayleyPoint.h"
#include "Utils.h"

CayleyPoint::CayleyPoint(int ID, std::vector<double> coordinates, ConstraintGraph* cg) {
	this->ID = ID;
	this->coordinates.assign(coordinates.begin(), coordinates.end());
	this->cg = cg;
}

CayleyPoint::~CayleyPoint() {
	coordinates.erase(coordinates.begin(), coordinates.end());
}

std::vector<double> CayleyPoint::getCoordinates() {
	return coordinates;
}


int CayleyPoint::getID() {
	return ID;
}

bool CayleyPoint::isValidCayleyPoint() {
	return validCayleyPoint;
}


std::ostream& operator<<(std::ostream& os, CayleyPoint &cp) {
	os<<"* ";
	for(double coor: cp.coordinates) {
		os<<coor<<" ";
	}
	os<<std::endl;
	return os;
}

bool CayleyPoint::getCayleyParameterValue(int v1, int v2, double* value) {
	int index = cg->getCayleyParameterIndex(v1, v2);
	if(index == -1) {
		std::cout<<"Cayley parameter ("<<v1<<", "<<v2<<
			") not found. Double Check"<<std::endl;
		return false;
	} 
	*value = coordinates[index];
	return true;
}

void CayleyPoint::computeRealizations(std::vector<std::vector<int> > tetras) {
	std::vector<bool> flipScheme;
	int numRealizations = pow(2, tetras.size());

	for(int i=0; i<numRealizations; i++) {
		convertFlipNumToFlipScheme(i, tetras.size());
		flipScheme = convertFlipNumToFlipScheme(i, tetras.size());
		computeRealization(tetras, flipScheme);
	}
}


void CayleyPoint::computeRealization(std::vector<std::vector<int> > tetras, std::vector<bool> flipScheme) {
	std::unordered_map< int, std::vector<double> >vertexLocations;
	//realizeFirstTetrahedron(tetras[0], flipScheme, vertexLocations);
	for(int i=1; i<tetras.size(); i++) {
		//realizeTetrahedron(tetras[i], flipScheme, vertexLocations);
	}
}

void CayleyPoint::realizeFirstTetrahedron(std::vector<int> tetrahedron, std::vector<bool> flipScheme,
				std::unordered_map< int, std::vector<double> >vertexLocations) {
	double d12, d13, d14, d23, d24, d34;

	std::vector<double> p1;
	std::vector<double> p2;
	std::vector<double> p3;
	std::vector<double> p4;
	
	std::vector<std::vector<double>> points;

	//Get all the edge lengths in the tetrahedron
	if(!(cg->getEdgeLength(tetrahedron[0], tetrahedron[1],&d12))){
		if(!getCayleyParameterValue(tetrahedron[0], tetrahedron[1],&d14)) {
			std::cout<<"Edge ("<<tetrahedron[0]<<", "<<tetrahedron[1]<<") not found. DoubleCheck"<<std::endl;
			exit(1);
		}
	}

	if(!(cg->getEdgeLength(tetrahedron[0], tetrahedron[2],&d13))){
		if(!(cg->getEdgeLength(tetrahedron[0], tetrahedron[2],&d12))){
			std::cout<<"Edge ("<<tetrahedron[0]<<", "<<tetrahedron[2]<<") not found. DoubleCheck"<<std::endl;
			exit(1);
		}
	}

	if(!(cg->getEdgeLength(tetrahedron[0], tetrahedron[3],&d14))){
		if(!getCayleyParameterValue(tetrahedron[0], tetrahedron[3],&d14)) {
			std::cout<<"Cayley parameter ("<<tetrahedron[0]<<", "<<tetrahedron[3]<<
				") not found. DoubleCheck"<<std::endl;
			exit(1);
		}
	}

	if(!(cg->getEdgeLength(tetrahedron[1], tetrahedron[2],&d23))){
		if(!(cg->getEdgeLength(tetrahedron[1], tetrahedron[2],&d12))){
			std::cout<<"Edge ("<<tetrahedron[1]<<", "<<tetrahedron[2]<<") not found. DoubleCheck"<<std::endl;
			exit(1);
		}
	}
	if(!(cg->getEdgeLength(tetrahedron[1], tetrahedron[3],&d24))){
		if(!(cg->getEdgeLength(tetrahedron[1], tetrahedron[3],&d12))){
			std::cout<<"Edge ("<<tetrahedron[1]<<", "<<tetrahedron[3]<<") not found. DoubleCheck"<<std::endl;
			exit(1);
		}
	}
	if(!(cg->getEdgeLength(tetrahedron[2], tetrahedron[3],&d34))){
		if(!(cg->getEdgeLength(tetrahedron[2], tetrahedron[3],&d12))){
			std::cout<<"Edge ("<<tetrahedron[2]<<", "<<tetrahedron[3]<<") not found. DoubleCheck"<<std::endl;
			exit(1);
		}
	}

	p1.push_back(0);
	p1.push_back(0);
	p1.push_back(0);
	vertexLocations.insert(make_pair(tetrahedron[0], p1));
	points.push_back(p1);
	
	p2.push_back(0);
	p2.push_back(0);
	p2.push_back(d12);
	vertexLocations.insert(make_pair(tetrahedron[1], p2));
	points.push_back(p2);

	double z3 = (pow(d12,2) + pow(d13,2) - pow(d23,2))/(2*d12);
	double y3_low = pow((d12 + z3),(1/2))*pow((d12 - z3),(1/2));
	double y3_high = -pow((d12 + z3),(1/2))*pow((d12 - z3),(1/2));
	p3.push_back(0);
	p3.push_back(z3);
	p3.push_back(y3_low);
	vertexLocations.insert(make_pair(tetrahedron[2], p3));
	points.push_back(p3);
}

void CayleyPoint::realizeTetrahedron(std::vector<int> tetrahedron, std::vector<bool> flipScheme, 
					std::unordered_map< int, std::vector<double> >vertexLocations) {
	double d12, d13, d14, d23, d24, d34; 

	std::vector<double> p1; 
	std::vector<double> p2;
	std::vector<double> p3;
	std::vector<double> p4; 
	std::vector<std::vector<double>> points; 
	
	//Get all the edge lengths in the tetrahedron
	if(!(cg->getEdgeLength(tetrahedron[0], tetrahedron[1],&d12))){
		if(!getCayleyParameterValue(tetrahedron[0], tetrahedron[1],&d14)) {
			std::cout<<"Edge ("<<tetrahedron[0]<<", "<<tetrahedron[1]<<") not found. DoubleCheck"<<std::endl;
			exit(1);
		}
	}

	if(!(cg->getEdgeLength(tetrahedron[0], tetrahedron[2],&d13))){
		if(!(cg->getEdgeLength(tetrahedron[0], tetrahedron[2],&d12))){
			std::cout<<"Edge ("<<tetrahedron[0]<<", "<<tetrahedron[2]<<") not found. DoubleCheck"<<std::endl;
			exit(1);
		}
	}

	if(!(cg->getEdgeLength(tetrahedron[0], tetrahedron[3],&d14))){
		if(!getCayleyParameterValue(tetrahedron[0], tetrahedron[3],&d14)) {
			std::cout<<"Cayley parameter ("<<tetrahedron[0]<<", "<<tetrahedron[3]<<
				") not found. DoubleCheck"<<std::endl;
			exit(1);
		}
	}

	if(!(cg->getEdgeLength(tetrahedron[1], tetrahedron[2],&d23))){
		if(!(cg->getEdgeLength(tetrahedron[1], tetrahedron[2],&d12))){
			std::cout<<"Edge ("<<tetrahedron[1]<<", "<<tetrahedron[2]<<") not found. DoubleCheck"<<std::endl;
			exit(1);
		}
	}
	if(!(cg->getEdgeLength(tetrahedron[1], tetrahedron[3],&d24))){
		if(!(cg->getEdgeLength(tetrahedron[1], tetrahedron[3],&d12))){
			std::cout<<"Edge ("<<tetrahedron[1]<<", "<<tetrahedron[3]<<") not found. DoubleCheck"<<std::endl;
			exit(1);
		}
	}
	if(!(cg->getEdgeLength(tetrahedron[2], tetrahedron[3],&d34))){
		if(!(cg->getEdgeLength(tetrahedron[2], tetrahedron[3],&d12))){
			std::cout<<"Edge ("<<tetrahedron[2]<<", "<<tetrahedron[3]<<") not found. DoubleCheck"<<std::endl;
			exit(1);
		}
	}

}
