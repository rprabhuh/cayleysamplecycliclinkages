#include <iostream>
#include "Utils.h"
#include <boost/program_options.hpp>
#include <glog/logging.h>

using namespace std;

string PROGRAM_NAME = "cycloalkane";
double CYCLOHEPTANE_BOND_ANGLE = 109.5;
double CYCLOOCTANE_BOND_ANGLE = 109.5;
double CYCLONONANE_BOND_ANGLE = 109.5;

double CYCLOHEPTANE_EDGE_LENGTH = 1.534;
double CYCLOOCTANE_EDGE_LENGTH = 1.534;
double CYCLONONANE_EDGE_LENGTH = 1.534;

double CYCLOHEPTANE_BOND_ANGLE_RADIINS = (CYCLOHEPTANE_BOND_ANGLE * PI / 180);
double CYCLOOCTANE_BOND_ANGLE_RADIINS = (CYCLOOCTANE_BOND_ANGLE * PI / 180);
double CYCLONONANE_BOND_ANGLE_RADIINS = (CYCLONONANE_BOND_ANGLE * PI / 180);

double CYCLOHEPTANE_DIAGONAL_EDGE_LENGTH = 2*sin(CYCLOHEPTANE_BOND_ANGLE_RADIINS/2)* CYCLOHEPTANE_EDGE_LENGTH;
double CYCLOOCTANE_DIAGONAL_EDGE_LENGTH = 2*sin(CYCLOOCTANE_BOND_ANGLE_RADIINS/2)*CYCLOOCTANE_EDGE_LENGTH ;
double CYCLONONANE_DIAGONAL_EDGE_LENGTH = 2*sin(CYCLONONANE_BOND_ANGLE_RADIINS/2) * CYCLONONANE_EDGE_LENGTH;


int handCommandLineArguments(int argc, char **argv, po::variables_map& Vmap) {
	//Handle Commandline arguments
	std::string commandUsage = "Usage: \n./" + PROGRAM_NAME + "--atoms <number of carbon atoms>\n";
	po::options_description OD("Command Line Arguments");
	OD.add_options() 
		("help", "Displays the help menu")
		("atoms", po::value<int>(), "The input number of carbon atoms in the cyclohexane molecule")
		("step-size", po::value<double>()->default_value(1.0), "The sampling step size")
		("log-level", po::value<int>()->default_value(0), "The input number of carbon atoms in the cyclohexane molecule");
	po::store(po::parse_command_line(argc, argv, OD), Vmap);
	po::notify(Vmap);

	try {
		if(Vmap.count("help")) {
			VLOG(2) << OD<< endl;
			cout << OD<< endl;
			return 1;
		}
		if(Vmap.count("atoms")) {
			VLOG(2) <<"Number of carbon atoms input = "<<Vmap["atoms"].as<int>() << "." << endl;
			cout <<"Number of carbon atoms input = "<<Vmap["atoms"].as<int>() << "." << endl;
		} else {
			VLOG(2) << commandUsage;
			cout << commandUsage;
			return 1;
		}

		if(Vmap.count("log-level")) {
			VLOG(2) << "Log level set to "<<Vmap["log-level"].as<int>() << "."<<endl;
			cout<< "Log level set to "<<Vmap["log-level"].as<int>() << "."<<endl;
		}

		if(Vmap.count("step-size")) {
			VLOG(2) << "Step size set to "<<Vmap["step-size"].as<double>() << "."<<endl;
			cout<< "Step size set to "<<Vmap["step-size"].as<double>() << "."<<endl;
		}
	} catch (const exception &ex) {
		std::cerr<<ex.what()<<endl;
		VLOG(2) << commandUsage;
		cout<< commandUsage;
		return 1;
	}
	return 0;
}

std::vector<bool> convertFlipNumToFlipScheme(int i, int numTetras) {
	int temp = i;
	std::vector<bool> flipScheme;

	for(int i=0; i<numTetras; i++) {
		flipScheme.push_back(false);
	}

	for(int j=0; j<flipScheme.size(); j++) {
		if(temp & 0x01){
			flipScheme[j]=true;
		}
		temp>>=1;
	}
	return flipScheme;
}
