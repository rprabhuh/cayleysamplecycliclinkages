#include "Orientation.h"
using namespace std;


int Orientation::getTetraFlipNum() {
	return tetraFlipNum;
}

int Orientation::getSubFlipNum() {
	return subFlipNum;
}

vector<double> Orientation::getFreeCayeleyParameters() {
	return freeCayeleyParameters;
}

vector<double> Orientation::getGlobalRigidityParameters() {
	return globalRigidityParameters;
}

void Orientation::setTetraFlipNum(int ) {
}

void Orientation::setSubFlipNum(int ) {
}

void Orientation::setFreeCayeleyParameters(vector<double> ) {
}

void Orientation::setGlobalRigidityParameters(vector<double>) {
}
