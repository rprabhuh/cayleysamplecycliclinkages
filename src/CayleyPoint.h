#ifndef CAYLEYPOINT_H_
#define CAYLEYPOINT_H_


#include<vector>
#include<iostream>
#include "ConstraintGraph.h"
#include "Orientation.h"
/*!
 * \class CayleyPoint
 *
 * \brief class for individual cayley configurations
 *
 * []
 *
 * \author Rahul Prabhu
 * \date
 */
class CayleyPoint {
public:
	/*!
	 *\brief CayleyPoint constructor
	 *
	 * \param[in] ID ID of the Cayley point
	 * \param[in] coordinates Coordinates of the Cayley parameters
	 * \param[in] cg Constraint Graph of the current region
 	 */
	CayleyPoint(int ID, std::vector<double> coordinates, ConstraintGraph* cg);

	/*!
	 *\brief CayleyPoint destructor
	 *
	 * \note Erases the coordinates array
 	 */
	~CayleyPoint();

	/*!
	 *\brief Returns the coordinates vector
	 *
	 * \return vector<double> of coordinates of the CayleyPoint
 	 */
	std::vector<double> getCoordinates();

	/*!
	 *\brief Return ID of the cayley point.
	 *
	 * \return int ID of the CayleyPoint
 	 */
	int getID();

	/*!
	 *\brief Computes the realization of the Cayley point
	 *
	 * Takes a Cayley point and finds the corresponding Cartesian point for it.
	 *
 	 */
	void computeRealizations(std::vector<std::vector<int> >);

	/*!
	 *\brief Computes the realization of the Cayley point
	 *
	 * Returns whether the current point is within tetrahedral bounds
	 *
	 * \return validCayleyPoint 
 	 */
	bool isValidCayleyPoint();

	friend std::ostream& operator<<(std::ostream& os, CayleyPoint &cp);

private:
	int ID;///<Identifier for the Cayley point
	bool validCayleyPoint;///<Identifies whether the current point is within tetrahedral bounds
	std::vector<double> coordinates;///<The Cayley coordinates or length values of the Cayley point
	std::vector<Orientation*> orientations;///<Vector of orientations of this Cayley point
	ConstraintGraph* cg;//Constraint graph of the region in which this is a Cayley point
	/*!
	 *\brief Computes the realization of the Cayley point
	 *
	 * Takes a Cayley point and finds the corresponding Cartesian point for it.
	 *
 	 */
	void computeRealization(std::vector<std::vector<int> > tetras, std::vector<bool> flipScheme);
	void realizeFirstTetrahedron(std::vector<int> tetrahedron, std::vector<bool> flipScheme, 
					std::unordered_map< int, std::vector<double> >vertexLocations); 
	void realizeTetrahedron(std::vector<int> tetrahedron, std::vector<bool> flipScheme, 
					std::unordered_map< int, std::vector<double> >vertexLocations); 
	bool getCayleyParameterValue(int, int, double*);
};

#endif
