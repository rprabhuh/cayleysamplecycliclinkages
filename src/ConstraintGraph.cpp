#include <iostream>
#include "ConstraintGraph.h"
#include "Utils.h"
#include <glog/logging.h>

using namespace std;

ConstraintGraph* ConstraintGraph::createCycloAlkane(int numAtoms) {
	if(numAtoms <= 6) {
		return {};
	} else if(numAtoms == 7) {
		VLOG(2)<<"Building cycloheptane molecule";
	} else if(numAtoms == 8) {
		VLOG(2)<<"Building cyclooctane molecule";
	} else if(numAtoms == 9) {
		VLOG(2)<<"Building cyclononane molecule";
	} else if(numAtoms > 9) {
		return {};
	}

	std::vector<std::pair<int,int> > edges;
	for(int i=0; i< numAtoms; i++) {
		int v1 = i%numAtoms;
		int v2 = (i+1)%numAtoms;
		int v3 = (i+2)%numAtoms;

		edges.push_back(make_pair(v1, v2));
		edges.push_back(make_pair(v1, v3));
	}

	return new ConstraintGraph(numAtoms, edges);
}

ConstraintGraph::ConstraintGraph(int numVertices, std::vector<std::pair<int,int> > edges) {
	this->numVertices =  numVertices;
	init_edge_lengths(edges);
}

ConstraintGraph::~ConstraintGraph() {
}

void ConstraintGraph::init_edge_lengths(std::vector<std::pair<int,int> > edges) {
	double edge_length = 0.0;
	double diagonal_length = 0.0;
	if(numVertices==7) {
		edge_length = CYCLOHEPTANE_EDGE_LENGTH;
		diagonal_length = CYCLOHEPTANE_DIAGONAL_EDGE_LENGTH;
	} else if(numVertices == 8) {
		edge_length = CYCLOOCTANE_EDGE_LENGTH;
		diagonal_length = CYCLOOCTANE_DIAGONAL_EDGE_LENGTH;
	} else if(numVertices == 9) {
		edge_length = CYCLONONANE_EDGE_LENGTH;
		diagonal_length = CYCLONONANE_DIAGONAL_EDGE_LENGTH;
	} else {
		cout<<"This graph is currently not supported"<<endl;
		exit(0);
	}

	for(std::pair<int, int> edge : edges) {
		if((edge.first+1)%numVertices == edge.second) {
			this->edges.emplace(edge, edge_length);
		} else if ((edge.first+2)%numVertices == edge.second) {
			this->edges.emplace(edge, diagonal_length);
		} else {
			cout<<"This graph is currently not supported"<<endl;
			exit(0);
		}
	}

}

void ConstraintGraph::drop_edges() {
	if(numVertices == 7) {
		dropped_edges.push_back(make_pair(2, 4));
		dropped_edges.push_back(make_pair(3, 5));
		dropped_edges.push_back(make_pair(3, 4));
	} else if(numVertices == 8) {
		dropped_edges.push_back(make_pair(3, 5));
		dropped_edges.push_back(make_pair(4, 5));
		dropped_edges.push_back(make_pair(4, 6));
	} else if(numVertices == 9) {
		cout<<"This graph is currently not supported"<<endl;
		exit(0);
	} else {
		cout<<"This graph is currently not supported"<<endl;
		exit(0);
	}

}

void ConstraintGraph::add_cayleyParameters() {
	if(numVertices == 7) {
		cayley_parameters.push_back(make_pair(0, 3));
		cayley_parameters.push_back(make_pair(0, 4));
		cayley_parameters.push_back(make_pair(1, 5));
		cayley_parameters.push_back(make_pair(2, 6));
		vector<int> t1;
		vector<int> t2;
		vector<int> t3;
		vector<int> t4;
		t1.push_back(0);
		t1.push_back(4);
		t1.push_back(5);
		t1.push_back(6);
		tetrahedra.push_back(t1);
		t2.push_back(0);
		t2.push_back(1);
		t2.push_back(5);
		t2.push_back(6);
		tetrahedra.push_back(t2);
		t3.push_back(0);
		t3.push_back(1);
		t3.push_back(2);
		t3.push_back(6);
		tetrahedra.push_back(t3);
		t4.push_back(0);
		t4.push_back(1);
		t4.push_back(2);
		t4.push_back(3);
		tetrahedra.push_back(t4);
	} else if(numVertices == 8) {
		cayley_parameters.push_back(make_pair(0, 3));
		cayley_parameters.push_back(make_pair(0, 5));
		cayley_parameters.push_back(make_pair(1, 4));
		cayley_parameters.push_back(make_pair(1, 6));
		cayley_parameters.push_back(make_pair(2, 7));
		vector<int> t1;
		vector<int> t2;
		vector<int> t3;
		vector<int> t4;
		vector<int> t5;
		t1.push_back(0);
		t1.push_back(1);
		t1.push_back(6);
		t1.push_back(7);
		tetrahedra.push_back(t1);
		t2.push_back(0);
		t2.push_back(1);
		t2.push_back(2);
		t2.push_back(7);
		tetrahedra.push_back(t2);
		t3.push_back(0);
		t3.push_back(1);
		t3.push_back(2);
		t3.push_back(3);
		tetrahedra.push_back(t3);
		t4.push_back(0);
		t4.push_back(5);
		t4.push_back(6);
		t4.push_back(7);
		tetrahedra.push_back(t4);
		t5.push_back(1);
		t5.push_back(2);
		t5.push_back(3);
		t5.push_back(4);
		tetrahedra.push_back(t5);
	} else if(numVertices == 9) {
		cout<<"This graph is currently not supported"<<endl;
		exit(0);
	} else {
		cout<<"This graph is currently not supported"<<endl;
		exit(0);
	}


}

//Getters and Setters
int ConstraintGraph::getNumVertices() {
	return numVertices;
}


int ConstraintGraph::getNumEdges() {
	return edges.size();
}

int ConstraintGraph::getNumCayleyParameters() {
	return cayley_parameters.size();
}

int ConstraintGraph::getNumDroppedEdges() {
	return dropped_edges.size();
}
std::unordered_map<std::pair<int, int>, double, boost::hash<std::pair<int, int>> > ConstraintGraph::getEdges() {
	return edges;
}

std::vector<std::pair<int, int>> ConstraintGraph::getDroppedEdges() {
	return dropped_edges;
}

std::vector<std::pair<int, int>> ConstraintGraph::getCayleyParameters() {
	return cayley_parameters;
}

std::vector<std::pair<double, double>> ConstraintGraph::getCayleyParameterRanges() {
	return cayleyParametersRanges;
}


std::vector<std::vector<int> > ConstraintGraph::getTetrahedra() {
	return tetrahedra;
}

bool ConstraintGraph::getEdgeLength(int a, int b, double* edgeLength) {
	std::unordered_map<std::pair<int, int>, double, boost::hash<std::pair<int, int>> >::iterator it = edges.find(make_pair(a, b));
	if(it != edges.end()) {
		*edgeLength = it->second;
		return true;
	}
	it = edges.find(make_pair(b, a));
	if(it != edges.end()) {
		*edgeLength = it->second;
		return true;
	}
	return false;

}

int ConstraintGraph::getCayleyParameterIndex(int v1, int v2) {
		std::pair<int, int> cParam1 = make_pair(v1, v2);
		std::pair<int, int> cParam2 = make_pair(v2, v1);
	for(int i=0; i<cayley_parameters.size(); i++) {
		if(cayley_parameters[i] == cParam1 || cayley_parameters[i] == cParam2) {
			return i;
		}
	}
	return -1;
}

void ConstraintGraph::generateCayleyParamRanges() {
		double paramLow=0;
		double paramHigh=0;
		double d10 = 0; 
		double d21 = 0; 
		double d32 = 0; 
		double d20 = 0; 
		double d31 = 0; 
		if(numVertices == 7) {
			d10 = CYCLOHEPTANE_EDGE_LENGTH;
			d21 = CYCLOHEPTANE_EDGE_LENGTH;
			d32 = CYCLOHEPTANE_EDGE_LENGTH;
			d20 = CYCLOHEPTANE_DIAGONAL_EDGE_LENGTH;
			d31 = CYCLOHEPTANE_DIAGONAL_EDGE_LENGTH;

		} else if(numVertices == 8) {
			d10 = CYCLOOCTANE_EDGE_LENGTH;
			d21 = CYCLOOCTANE_EDGE_LENGTH;
			d32 = CYCLOOCTANE_EDGE_LENGTH;
			d20 = CYCLOOCTANE_DIAGONAL_EDGE_LENGTH;
			d31 = CYCLOOCTANE_DIAGONAL_EDGE_LENGTH;
		} else {
			cout<<"Molecule not supported."<<endl;
			VLOG(google::INFO)<<"Molecule not supported."<<endl;
			exit(1);
		}

		paramHigh = (2*d10*d21 - d10*d31 + 2*d20*d21 + d10*d32 + d20*d31 -
			d20*d32 + d21*d31 + 3*d21*d32 + pow((4*pow(d10,2)*pow(d21,2) -
			4*pow(d10,2)*d21*d31 - 12*pow(d10,2) *d21*d32 + pow(d10,2)*pow(d31,2) -
			2*pow(d10,2)*d31*d32 + pow(d10,2) * pow(d32,2) - 8*d10*d20*pow(d21,2) +
			8*d10*d20*d21*d31 + 24*d10*d20*d21*d32 - 2*d10*d20*pow(d31,2) +
			4*d10*d20*d31*d32 - 2*d10*d20*pow(d32,2) - 8*d10*pow(d21,3) +
			8*d10*pow(d21,2)*d31 + 24*d10*pow(d21,2)*d32 - 2*d10*d21*pow(d31,2) +
			4*d10*d21*d31*d32 - 2*d10*d21*pow(d32,2) + 4*pow(d20,2)*pow(d21,2) -
			4*pow(d20,2)*d21*d31 - 12*pow(d20,2)*d21*d32 + pow(d20,2)*pow(d31,2) -
			2*pow(d20,2)*d31*d32 + pow(d20,2)*pow(d32,2) - 8*d20*pow(d21,3) +
			8*d20*pow(d21,2) * d31 + 24*d20*pow(d21,2)*d32 + 2*d20*d21*pow(d31,2) -
			4*d20*d21*d31*d32 + 2*d20*d21*pow(d32,2) + 4*pow(d21,4) - 4*pow(d21,3)*d31 -
			12*pow(d21,3)*d32 + pow(d21,2)*pow(d31,2) - 2*pow(d21,2) *d31*d32 +
			pow(d21, 2)*pow(d32,2)),0.5) - 2*pow(d21,2))/(4*d21);

		paramLow =  (2*d10*d21 - d10*d31 + 2*d20*d21 + d10*d32 + d20*d31 -
			d20*d32 + d21*d31 + 3*d21*d32 - pow((4*pow(d10,2)*pow(d21,2) -
			4*pow(d10,2)*d21*d31 - 12*pow(d10,2)*d21*d32 + pow(d10,2)*pow(d31,2) -
			2*pow(d10,2)*d31*d32 + pow(d10,2)*pow(d32,2) - 8*d10*d20*pow(d21,2) +
			8*d10*d20*d21*d31 + 24*d10*d20*d21*d32 - 2*d10*d20*pow(d31,2) +
			4*d10*d20*d31*d32 - 2*d10*d20*pow(d32,2) - 8*d10*pow(d21,3) +
			8*d10*pow(d21,2)*d31 + 24*d10*pow(d21,2)*d32 - 2*d10*d21*pow(d31,2) +
			4*d10*d21*d31*d32 - 2*d10*d21*pow(d32,2) + 4*pow(d20,2)*pow(d21,2) -
			4*pow(d20,2)*d21*d31 - 12*pow(d20,2)*d21*d32 + pow(d20,2)*pow(d31,2) -
			2*pow(d20,2)*d31*d32 + pow(d20,2)*pow(d32,2) - 8*d20*pow(d21,3) +
			8*d20*pow(d21,2)*d31 + 24*d20*pow(d21,2)*d32 + 2*d20*d21*pow(d31,2) -
			4*d20*d21*d31*d32 + 2*d20*d21*pow(d32,2) + 4*pow(d21,4) - 4*pow(d21,3)*d31 -
			12*pow(d21,3)*d32 + pow(d21,2)*pow(d31,2) - 2*pow(d21,2)*d31*d32 +
			pow(d21,2)*pow(d32,2)), 0.5) - 2*pow(d21,2))/(4*d21);

		VLOG(1)<<"ParamHigh = "<<paramHigh;
		VLOG(1)<<"ParamLow = "<<paramLow;

		if(paramHigh < paramLow) {
			double temp = paramHigh;
			temp = paramLow;
			paramLow = paramHigh;
			paramHigh = temp;
		}

		for(auto it: cayley_parameters) {
			this->cayleyParametersRanges.push_back(make_pair(paramLow, paramHigh));
		}
}
