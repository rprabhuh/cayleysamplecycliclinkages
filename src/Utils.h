#ifndef UTILS_H_
#define UTILS_H_
#include <vector>
#include <cmath>
#include <boost/program_options.hpp>
#include "ConstraintGraph.h"

namespace po = boost::program_options;
#define PI 3.14159265

extern double CYCLOHEPTANE_BOND_ANGLE; 
extern double CYCLOOCTANE_BOND_ANGLE;
extern double CYCLONONANE_BOND_ANGLE;

extern double CYCLOHEPTANE_EDGE_LENGTH; 
extern double CYCLOOCTANE_EDGE_LENGTH; 
extern double CYCLONONANE_EDGE_LENGTH;

extern double CYCLOHEPTANE_BOND_ANGLE_RADIINS; 
extern double CYCLOOCTANE_BOND_ANGLE_RADIINS; 
extern double CYCLONONANE_BOND_ANGLE_RADIINS;

extern double CYCLOHEPTANE_DIAGONAL_EDGE_LENGTH;
extern double CYCLOOCTANE_DIAGONAL_EDGE_LENGTH;
extern double CYCLONONANE_DIAGONAL_EDGE_LENGTH;

int handCommandLineArguments(int argc, char **argv, po::variables_map& Vmap);


std::vector<bool> convertFlipNumToFlipScheme(int i, int numTetras);

#endif

