# CayleySampleCyclicGraphs

Description
============

This is a mathematical software that formally model the energy landscapes of cycloalkanes (monocyclic saturated hydorcarbons). Our novel geometric methodology is based on the theory of convex Cayley (distance-based) parametrization. Cycloalkane molecules are represented as bar-and-joint geometric constraint systems with the atoms being the joints, and the bondlengths and bond angles represent constraints between the atoms. Using this setup, the energy landscape of the cyclo alkane can be thought of as the solution space or configuration space to this geometric constraint system.

Requirements
============
These are configurations with which the software has been tested. It may work on other equivalent platforms/libraries, but hasn't been tested.

- Ubuntu 18.04 or higher
- CMake version 3.10.2 or higher
	-sudo apt install cmake
- g++ version 9.2.1 or higher
	- sudo apt install g++
- Boost c++ libraries version 1.4 or higher
	- sudo apt install libboost-all-dev

Optionally

- Google logging module glog version 0.4 or higher for logging
	- https://codeyarns.com/2017/10/26/how-to-install-and-use-glog/
- Google Test framework for running test cases
	- https://gist.github.com/Cartexius/4c437c084d6e388288201aadf9c8cdd5

Installation
============
- $ mkdir build
- $ cd build
- $ cmake ..
- $ ./cycloalkane


