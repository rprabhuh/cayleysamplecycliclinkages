#include "gtest/gtest.h"
#include <boost/program_options.hpp>
#include <glog/logging.h>

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);

	// Initialize Google's logging library.
    google::InitGoogleLogging(argv[0]);
    FLAGS_log_dir = ".";
    FLAGS_v = 2;
	
	return RUN_ALL_TESTS();
}
