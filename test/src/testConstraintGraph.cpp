#include "gtest/gtest.h"

#include <Utils.h>
#include <ConstraintGraph.h>

TEST(ConstraintGraph, testCylcoheptane) {
		ConstraintGraph* cycloheptane = ConstraintGraph::createCycloAlkane(7);
		ASSERT_NE(cycloheptane, nullptr);
		int vertices = cycloheptane->getNumVertices();
		EXPECT_EQ(vertices, 7);
		delete cycloheptane;
}

TEST(ConstraintGraph, testCylcooctane) {
		ConstraintGraph* cyclooctane = ConstraintGraph::createCycloAlkane(8);
		ASSERT_NE(cyclooctane, nullptr);
		int vertices = cyclooctane->getNumVertices();
		EXPECT_EQ(vertices, 8);
		delete cyclooctane;
}

TEST(ConstraintGraph, testCylcononane) {
		ConstraintGraph* cyclononane= ConstraintGraph::createCycloAlkane(9);
		ASSERT_NE(cyclononane, nullptr);
		int vertices = cyclononane->getNumVertices();
		EXPECT_EQ(vertices, 9);
		delete cyclononane;
}

TEST(ConstraintGraph, testUnsupportedMolecule) {
		ConstraintGraph* cyclomistake = ConstraintGraph::createCycloAlkane(6);
		EXPECT_EQ(cyclomistake, nullptr);
		cyclomistake = ConstraintGraph::createCycloAlkane(10);
		EXPECT_EQ(cyclomistake, nullptr);
		delete cyclomistake;
}

TEST(ConstraintGraph, drop_edges_add_CayleyParameters_cycloheptane) {
		ConstraintGraph* cycloheptane= ConstraintGraph::createCycloAlkane(7);
		ASSERT_NE(cycloheptane, nullptr);
		int vertices = cycloheptane->getNumVertices();
		EXPECT_EQ(vertices, 7);
		int edges = cycloheptane->getNumEdges();
		EXPECT_EQ(edges, 14);
		cycloheptane->drop_edges();
		int dropped_edges = cycloheptane->getNumDroppedEdges();
		EXPECT_EQ(dropped_edges, 3);
		cycloheptane->add_cayleyParameters();
		int cayleyParameters = cycloheptane->getNumCayleyParameters();
		EXPECT_EQ(cayleyParameters, 4);

		delete cycloheptane;
}

TEST(ConstraintGraph, drop_edges_add_CayleyParameters_cyclooctane) {
		ConstraintGraph* cyclooctane= ConstraintGraph::createCycloAlkane(8);
		ASSERT_NE(cyclooctane, nullptr);
		int vertices = cyclooctane->getNumVertices();
		EXPECT_EQ(vertices, 8);
		int edges = cyclooctane->getNumEdges();
		EXPECT_EQ(edges, 16);
		cyclooctane->drop_edges();
		int dropped_edges = cyclooctane->getNumDroppedEdges();
		EXPECT_EQ(dropped_edges, 3);
		cyclooctane->add_cayleyParameters();
		int cayleyParameters = cyclooctane->getNumCayleyParameters();
		EXPECT_EQ(cayleyParameters, 5);

		delete cyclooctane;
}
